package com.example.stus.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //если вьюха кастомная и нам ее можно пихнуть на весь экрать то можно сделать это так
        setContentView(new Draw(getApplicationContext()));
    }

}

