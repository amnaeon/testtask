package com.example.stus.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class Draw extends View {
    private float x = 0;
    private float y = 0;
    private Paint paint;

    //После вызова данного конструктора автоматически вызывается метод onDraw, именно по этому там и стоит проверка,
    //на самом деле проверка гамно, ну демку я писал минуту и мне было лень делать все красиво, мне главное чтоб ты уловил суть
    public Draw(Context context) {
        super(context);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.YELLOW);
    }

    public void onDraw(Canvas c) {
        if (x != 0 && y != 0)
            c.drawCircle(x, y, 50, paint);
    }

    //ну про ивенты можешь для общего развития почитать, ну сдесь тебе главное понять что после каждого касания идет перерисовка
    //всего всего(тобишь прийдется тебе при создании архитектуры продуммать этот момент(тоесть както объеденить результаты всех
    //инструментов в одну сущность. Мур*))
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_DOWN) {
            x = event.getX();
            y = event.getY();
            //метод который вызывает перерисовку(onDraw())
            invalidate();
        }
        return true;
    }
}